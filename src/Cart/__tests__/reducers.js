import { cartReducers } from '../reducers';
import { addToCart } from '../actions';

describe('cartReducers', () => {
  describe('without param', () => {
    it('returns initial state', () => {
      expect(cartReducers()).toEqual([]);
    });
  });

  describe('with a correct initial state', () => {
    const initialState = [{ productId: 1, quantity: 2 }];

    describe('without action', () => {
      it('returns initial state', () => {
        expect(cartReducers(initialState)).toEqual(initialState);
      });
    });

    describe('with a correct action', () => {
      describe('on an existing product', () => {
        const action = addToCart(1, 3);

        it('returns correct state', () => {
          expect(cartReducers(initialState, action)).toEqual([{ productId: 1, quantity: 5 }]);
        });
      });

      describe('adding a new product', () => {
        const action = addToCart(2, 2);

        it('returns correct state', () => {
          expect(cartReducers(initialState, action)).toEqual([
            { productId: 1, quantity: 2 },
            { productId: 2, quantity: 2 },
          ]);
        });
      });
    });
  });
});
