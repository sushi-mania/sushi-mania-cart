import { add, find, propEq } from 'ramda';

import { ADD_TO_CART } from './actions';

const initialState = [];

const hasProductId = (productId, state) => find(propEq('productId', productId), state);

const addQuantity = (payload, state) =>
  state.map(({ productId, quantity }) => ({
    productId,
    quantity: propEq('productId', productId)(payload) ? add(quantity, payload.quantity) : quantity,
  }));

export const cartReducers = (state = initialState, { payload, type } = { payoad: null, type: null }) => {
  switch (type) {
    case ADD_TO_CART:
      return hasProductId(payload.productId, state) ? addQuantity(payload, state) : [...state, payload];

    default:
      return state;
  }
};
