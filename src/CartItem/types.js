import { number, string } from 'prop-types';

export default {
  price: number,
  quantity: number,
  reference: string,
  title: string,
};
