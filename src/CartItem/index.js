import { Button, Selector } from '@sushi-mania/ui';
import { func } from 'prop-types';
import React, { Component } from 'react';

import { CartItem, CartItemTitle, CartItemOption } from './styles';
import types from './types';

class Cart extends Component {
  constructor(props) {
    super(props);

    const { quantity } = this.props;

    this.state = {
      selectedQuantity: quantity,
    };
  }

  handleRemoveButtonClick = () => {
    const { reference, removeProduct } = this.props;

    removeProduct(reference);
  };

  handleQuantitySelectorChange = event => {
    if (event && event.target && event.target.value) {
      const selectedQuantity = parseInt(event.target.value, 10);

      this.setState(
        () => ({
          selectedQuantity,
        }),
        this.updateProductQuantity,
      );
    }
  };

  updateProductQuantity() {
    const { quantity, reference, updateProductQuantity } = this.props;
    const { selectedQuantity } = this.state;
    const deltaQuantity = selectedQuantity - quantity;

    updateProductQuantity(reference, deltaQuantity);
  }

  renderButton = () => (
    <div className="Cart-item-remove-button">
      <Button action={this.handleRemoveButtonClick} size="small">
        Supprimer
      </Button>
    </div>
  );

  renderQuantitySelector() {
    const { quantity } = this.props;
    const availableQuantities = [1, 2, 3, 4, 5, 10];
    const extendedAvailableQuantities = availableQuantities.includes(quantity)
      ? availableQuantities
      : [...availableQuantities, quantity];
    const sortedAvailableQuantites = extendedAvailableQuantities.sort((a, b) => a - b);
    const options = sortedAvailableQuantites.map(qty => ({
      label: qty.toString(),
      value: qty.toString(),
    }));

    return (
      <Selector value={quantity.toString()} onChange={this.handleQuantitySelectorChange} options={options} />
    );
  }

  render() {
    const { price, quantity, reference, title } = this.props;

    return (
      <CartItem key={reference}>
        <CartItemTitle>{`${reference} - ${title}`}</CartItemTitle>
        <CartItemOption>{this.renderQuantitySelector()}</CartItemOption>
        <CartItemOption>{`${price.toFixed(2)} €`}</CartItemOption>
        <CartItemOption>{`${(quantity * price).toFixed(2)} €`}</CartItemOption>
        {this.renderButton()}
      </CartItem>
    );
  }
}

Cart.defaultProps = {
  price: 0,
  quantity: 0,
  reference: '',
  removeProduct: Function.prototype,
  title: '',
  updateProductQuantity: Function.prototype,
};

Cart.propTypes = {
  ...types,
  removeProduct: func,
  updateProductQuantity: func,
};

export { default as types } from './types';

export default Cart;
