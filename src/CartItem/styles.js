import styled from 'styled-components';

export const CartItem = styled.li({
  display: 'flex',
  alignItems: 'center',
  paddingBottom: '1em',
  paddingTop: '1em',
});

export const CartItemTitle = styled.div({
  flex: 3,
});

export const CartItemOption = styled.div({
  flex: 1,
});
