import { cartCount } from '../selectors';

describe(cartCount, () => {
  test('with no cart param', () => {
    expect(cartCount()).toBe(0);
  });

  test('with no cart param and a defaultCount', () => {
    expect(cartCount(null, 34)).toBe(34);
  });

  test('with a not array cart', () => {
    expect(cartCount('az')).toBe(0);
    expect(cartCount(12)).toBe(0);
  });

  test('with an uncorrect cart array', () => {
    expect(cartCount([])).toBe(0);
    expect(cartCount([{ foo: 'bar' }])).toBe(0);
  });

  test('with a correct cart array', () => {
    expect(cartCount([{ quantity: 1 }, { quantity: 2 }, { quantity: 3 }])).toBe(6);
  });
});
