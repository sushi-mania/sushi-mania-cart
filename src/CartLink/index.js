import { connect } from 'react-redux';

import CartLink from './component';
import { cartCount } from './selectors';

const mapStateToProps = state => ({
  cartCount: cartCount(state.cart),
});

export default connect(mapStateToProps)(CartLink);
