import { number } from 'prop-types';
import React from 'react';

import { StyledLink } from './styles';

const CartLink = ({ cartCount }) => {
  if (cartCount === 0) return 'Panier vide';

  return <StyledLink to="/my-cart">{`Voir mon panier - ${cartCount} produit(s)`}</StyledLink>;
};

CartLink.propTypes = {
  cartCount: number,
};

CartLink.defaultProps = {
  cartCount: 0,
};

export default CartLink;
