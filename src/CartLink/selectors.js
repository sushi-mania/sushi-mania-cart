import { isNotArray } from 'ramda-adjunct';

export const cartCount = (cart, defaultCount = 0) =>
  isNotArray(cart) ? defaultCount : cart.reduce((sum, { quantity = 0 }) => sum + quantity, 0);
