import babel from 'rollup-plugin-babel';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import resolve from 'rollup-plugin-node-resolve';

export default {
  input: 'src/index.js',
  output: {
    file: 'dist/index.js',
    format: 'esm',
  },
  plugins: [
    peerDepsExternal({
      includeDependencies: true,
    }),
    resolve(),
    babel({
      exclude: 'node_modules/**',
    }),
  ],
};
